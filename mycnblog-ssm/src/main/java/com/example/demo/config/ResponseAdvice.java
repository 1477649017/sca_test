package com.example.demo.config;

import com.example.demo.common.AjaxResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 14776
 * Date: 2023-05-24
 * Time: 21:02
 */

/*
* 实现数据返回的保底类  万一忘记了调用AjaxResult的统一格式返回的方法 这里可以做保底
* 在返回数据之前，先判断时候是统一格式的对象了，如果不是，手动封装一下
* */
@ControllerAdvice
public class ResponseAdvice implements ResponseBodyAdvice {
    @Autowired
    ObjectMapper objectMapper;
    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        return true;//开关打开
    }

    @SneakyThrows   //注解声明一个异常
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        //对数据格式进行校验
        if(body instanceof AjaxResult){
            return body;//数据已经是统一格式了 直接返回
        }
        if(body instanceof String){
            //字符串类型需要特殊处理
            objectMapper.writeValueAsString(AjaxResult.success(body));//将body封装为AjaxResult对象然后转为json字符串返回
        }
        //如果既不是字符串 也不是统一格式类型
        return AjaxResult.success(body);//转为统一格式对象返回
    }
}
