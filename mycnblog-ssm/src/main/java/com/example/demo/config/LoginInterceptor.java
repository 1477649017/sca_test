package com.example.demo.config;

import com.example.demo.common.AppVariable;
import org.apache.ibatis.plugin.Interceptor;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 14776
 * Date: 2023-05-25
 * Time: 14:36
 */
/*
* 定义一个普通的拦截器类
* */
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession httpSession = request.getSession(false);
        if(httpSession != null && httpSession.getAttribute(AppVariable.USER_SESSION_KEY) != null){
            //如果session存在 则是登录了的
            return true;
        }
        //如果检测是未登录 就重定向到登录页
        response.sendRedirect("/blog_login.html");
        return false;
    }
}
