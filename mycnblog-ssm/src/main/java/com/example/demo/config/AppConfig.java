package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 14776
 * Date: 2023-05-25
 * Time: 14:41
 */
/*
* 全局的配置类
* */
@Configuration
public class AppConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //定义我们的拦截规则
        //弄清楚什么要拦 什么不拦
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/css/**")
                .excludePathPatterns("/editor.md/**")
                .excludePathPatterns("/img/**")
                .excludePathPatterns("/js/**")
                .excludePathPatterns("/blog_login.html")
                .excludePathPatterns("/blog_detail.html")
                .excludePathPatterns("/public_list.html")
                .excludePathPatterns("/public_list_datail.html")
                .excludePathPatterns("/find_password.html")
                .excludePathPatterns("/article/getblog")
                .excludePathPatterns("/article/uprcount")
                .excludePathPatterns("/article/getuser")
                .excludePathPatterns("/article/listbypage")
                .excludePathPatterns("/user/login")
                .excludePathPatterns("/user/getcode")
                .excludePathPatterns("/user/sendcode")
                .excludePathPatterns("/user/updatepassword")
                .excludePathPatterns("/user/reg");
    }
}
