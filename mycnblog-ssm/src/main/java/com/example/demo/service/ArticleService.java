package com.example.demo.service;

import com.example.demo.entity.ArticleInfo;
import com.example.demo.mapper.ArticleMapper;
import com.example.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 14776
 * Date: 2023-05-25
 * Time: 15:30
 */
@Service
public class ArticleService {
    @Autowired
    ArticleMapper articleMapper;

    public int getArtCountByUid(Integer uid){
        return articleMapper.getArtCountByUid(uid);
    }

    public List<ArticleInfo> getBlogs(Integer uid){
        return articleMapper.getBlogs(uid);
    }

    public int getUid(Integer blogId){
        return articleMapper.getUid(blogId);
    }

    public ArticleInfo getBlog(Integer blogId){
        return articleMapper.getBlog(blogId);
    }

    public int delete(Integer blogId,Integer uid){
        return articleMapper.delete(blogId,uid);
    }

    public int updateRcount(Integer blogId){
        return articleMapper.updateRcount(blogId);
    }

    public int add(ArticleInfo articleInfo){
        return articleMapper.add(articleInfo);
    }

    public int updateArt(ArticleInfo articleInfo){
        return articleMapper.updateArt(articleInfo);
    }


    public List<ArticleInfo>  pagingQuery(Integer psize,Integer offsize){
        return articleMapper.pagingQuery(psize,offsize);
    }

    public int getCount(){
        return articleMapper.getCount();
    }
 }
