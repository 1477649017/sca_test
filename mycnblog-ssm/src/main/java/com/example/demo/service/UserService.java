package com.example.demo.service;

import com.example.demo.entity.UserInfo;
import com.example.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 14776
 * Date: 2023-05-25
 * Time: 10:45
 */
@Service
public class UserService {
    @Autowired
    UserMapper userMapper;

    public int reg(UserInfo userInfo){
        return userMapper.reg(userInfo);
    }

    public UserInfo getUserByName(String username){
        return userMapper.getUserByName(username);
    }

    public UserInfo getUserById(Integer id){
        return userMapper.getUserById(id);
    }

    public int updateInfo(UserInfo userInfo){
        return userMapper.updateInfo(userInfo);
    }

    public UserInfo getUserByPhone(String phone){
       return userMapper.getUserByPhone(phone);
    }

    public int updatePassword(String password,String phone){
        return userMapper.updatePassword(password,phone);
    }
}
