package com.example.demo.common;

import com.example.demo.entity.UserInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 14776
 * Date: 2023-05-25
 * Time: 16:59
 */
/*
* 用于获取当前会话对象
* */
public class UserSessionUtils {
    public static UserInfo getUser(HttpServletRequest request){
        HttpSession httpSession = request.getSession(false);//拿到当前会话
        if(httpSession != null && httpSession.getAttribute(AppVariable.USER_SESSION_KEY) != null){
            //返回当前登陆的对象
            return (UserInfo) httpSession.getAttribute(AppVariable.USER_SESSION_KEY);
        }
        return null;
    }
}
