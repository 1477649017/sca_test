package com.example.demo.common;

import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 14776
 * Date: 2023-05-26
 * Time: 22:33
 */
/*
* 提供加密 解密的相关方法类
* */
public class PasswordUtils {

    //加盐生成密码
    public static String encrypt(String password){
        //传递过来的是基础的明文密码
        //1，生成盐值 32位的uuid uuid是36位 中间有四个横杠
        String salt = UUID.randomUUID().toString().replace("-","");
        //2，生成加盐后的加密密码 利用框架提供的方法进行md5加密
        String saltPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes());
        //3，生成最终的密码 也就是保存在数据库中的密码 格式为 盐值(32位) + $(分隔符) + saltPassword(32位)
        String finalPassword = salt + "$" + saltPassword;
        return finalPassword;
    }

    //根据提供的盐值 进行加密操作 用于后续的验证操作
    /*
    * password是明文密码 salt是盐值
    * */
    public static String encrypt(String password,String salt){
        //此方法就是上面方法的一个重载 只不过此时不用自己生成盐值 而是根据提供的盐值进行加密
        String saltPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes());
        //3，生成最终的密码 也就是保存在数据库中的密码 格式为 盐值(32位) + $(分隔符) + saltPassword(32位)
        String finalPassword = salt + "$" + saltPassword;
        return finalPassword;
    }


    /*
    *
    *验证密码的方法 对比你加密之后的最终密码与数据库中的密码
    * inputPassword 是用户输入的明文密码 finalPassword是数据库中存储的密码
    * */
    public static boolean check(String inputPassword,String finalPassword){
        //首先参数校验
        if(StringUtils.hasLength(inputPassword) && StringUtils.hasLength(finalPassword) && finalPassword.length() == 65){
            //密码都要存在 并且数据库中的密码长度是一定的
            //获取盐值
            String salt = finalPassword.split("\\$")[0];//要对$进行转义 因为它是一个关键字，不然会分割失败
            //根据拿到的盐值对传入的密码进行加密
            String comfirmPassword = encrypt(inputPassword,salt);//生成一个待验证的最终密码
            //进行验证
            return comfirmPassword.equals(finalPassword);
        }
        return false;
    }


}
