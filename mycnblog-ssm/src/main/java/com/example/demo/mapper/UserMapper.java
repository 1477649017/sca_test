package com.example.demo.mapper;

import com.example.demo.entity.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 14776
 * Date: 2023-05-25
 * Time: 10:44
 */
@Mapper
public interface UserMapper {
    int reg(UserInfo userInfo);//注册

    //alter table userinfo add unique(username); 首先在mysql中给username添加唯一约束
    UserInfo getUserByName(@Param("username") String username);//登录

    UserInfo getUserById(@Param("id") Integer id);

    int updateInfo(UserInfo userInfo);

    UserInfo getUserByPhone(@Param("phone") String phone);

    int updatePassword(@Param("password") String password,@Param("phone") String phone);
}
