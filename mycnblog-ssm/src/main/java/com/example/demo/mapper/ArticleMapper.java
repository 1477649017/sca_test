package com.example.demo.mapper;

import com.example.demo.entity.ArticleInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 14776
 * Date: 2023-05-25
 * Time: 15:28
 */
@Mapper
public interface ArticleMapper {
    int getArtCountByUid(@Param("uid") Integer uid);

    List<ArticleInfo> getBlogs(@Param("uid") Integer uid);

    int getUid(@Param("blogId") Integer blogId);

    ArticleInfo getBlog(@Param("blogId") Integer blogId);

    int delete(@Param("blogId") Integer blogId,@Param("uid") Integer uid);

    int updateRcount(@Param("blogId") Integer blogId);

    int add(ArticleInfo articleInfo);

    int updateArt(ArticleInfo articleInfo);

    List<ArticleInfo> pagingQuery(@Param("psize") Integer psize,@Param("offsize") Integer offsize);

    int getCount();
}
