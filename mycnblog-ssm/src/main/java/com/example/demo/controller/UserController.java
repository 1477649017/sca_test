package com.example.demo.controller;
import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.captcha.generator.RandomGenerator;
import com.example.demo.common.AjaxResult;
import com.example.demo.common.AppVariable;
import com.example.demo.common.PasswordUtils;
import com.example.demo.common.UserSessionUtils;
import com.example.demo.config.Sample;
import com.example.demo.entity.UserInfo;
import com.example.demo.entity.vo.UserInfoVO;
import com.example.demo.service.ArticleService;
import com.example.demo.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 14776
 * Date: 2023-05-25
 * Time: 10:46
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    ArticleService articleService;
    @RequestMapping("/reg")
    public AjaxResult reg(UserInfo userInfo){
        //首先进行非空校验 参数有效性校验
        //后端记住前端数据不可信 即时前端js代码中进行了非空校验 但是前端代码是暴露的 所以利用三方工具也是可以请求的，绕开
        //前端的校验
        if(userInfo == null || !StringUtils.hasLength(userInfo.getUsername()) || !StringUtils.hasLength(userInfo.getPassword())){
            return AjaxResult.fail(-1,"非法参数");//调用失败请求的统一格式返回
        }
        //对password进行加密
        userInfo.setPassword(PasswordUtils.encrypt(userInfo.getPassword()));
        return AjaxResult.success(userService.reg(userInfo));
    }

    @RequestMapping("/login")
    public AjaxResult login(HttpServletRequest httpServletRequest,String username, String password,String code){
        //参数校验
        if(!StringUtils.hasLength(username) || !StringUtils.hasLength(password)){
            return AjaxResult.fail(-1,"非法参数");//调用失败请求的统一格式返回
        }
        //查询数据库
        UserInfo userInfo = userService.getUserByName(username);
        if(userInfo != null && userInfo.getId() > 0){
            //说明更据用户名查询到了用户 是有效用户
            //开始校验密码与验证码
            if(PasswordUtils.check(password,userInfo.getPassword()) && AppVariable.textCode.equals(code.toLowerCase())){//验证加密
                //登录成功
                userInfo.setPassword("");//返回前端之前隐藏敏感信息
                //创建会话存储登录信息
                HttpSession httpSession = httpServletRequest.getSession(true);
                httpSession.setAttribute(AppVariable.USER_SESSION_KEY,userInfo);
                return AjaxResult.success(userInfo);//返回data
            }
        }
        //否则要么是无效用户 或者密码错误
        return AjaxResult.fail(-1,"密码匹配错误",null);
    }

    @RequestMapping("/showinfo")
    public AjaxResult getUserinfo(HttpServletRequest request){
        //首先得到当前用户 然后再去查询文章总数 最后构造vo对象
        UserInfo userInfo = userService.getUserByName(UserSessionUtils.getUser(request).getUsername());
        if(userInfo == null){
            AjaxResult.fail(-1,"非法请求");
        }
        //通过Spring提供的深克隆方法 复制userinfo对象给userinfovo
        UserInfoVO userInfoVO =  new UserInfoVO();
        BeanUtils.copyProperties(userInfo,userInfoVO);
        //通过当前用户id查询总数
        Integer artCount = articleService.getArtCountByUid(userInfo.getId());
        userInfoVO.setArtCount(artCount);
        return AjaxResult.success(userInfoVO);
    }

    /*
    * 注销登录
    * */
    @RequestMapping("/logout")
    public AjaxResult logout(HttpServletRequest request){
        HttpSession httpSession = request.getSession(false);
        httpSession.removeAttribute(AppVariable.USER_SESSION_KEY);//移除当前会话
        return AjaxResult.success(null);
    }

    @RequestMapping("/upheadphoto")
    public AjaxResult uploadPhoto(@RequestPart("headpicture") MultipartFile file) throws IOException {
        if(file.isEmpty()){
            return AjaxResult.fail(-1,"上传文件为空!");
        }
        //首先定义我们将要存储头像的根路径
        String path = "src/main/resources/static/img/";
        //通过UUID生成唯一的文件名
        path += UUID.randomUUID().toString().replace("-","");
        //然后获取文件的后缀名
        path += file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        //然后将文件保存到指定目录下 transferTo 这里必须要使用绝对路径 不然走的就是temp目录的相对路径 是找不到的
        File currFile = new File(new File(path).getAbsolutePath());
        file.transferTo(currFile);//将文件写入指定路径
        //构造返回给前端的路径
        String retPath = "/img/" + currFile.getAbsolutePath().substring(currFile.getAbsolutePath().lastIndexOf("\\") + 1);//注意路径分隔符
        return AjaxResult.success(retPath);//将文件最终的路径返回给前端
    }


    @RequestMapping("/getinfo")
    public AjaxResult getInfo(HttpServletRequest request){
        //首先得到当前用户
        UserInfo userInfo = UserSessionUtils.getUser(request);
        if(userInfo == null){
            AjaxResult.fail(-1,"非法请求");
        }
        //然后通过用户名获取用户
        UserInfo userInfo1 = userService.getUserByName(userInfo.getUsername());
        userInfo1.setPassword("");//隐藏敏感信息
        return AjaxResult.success(userInfo1);
    }

    @RequestMapping("/updateinfo")
    public AjaxResult updateInfo(UserInfo userInfo,HttpServletRequest request){
        if(userInfo == null || !StringUtils.hasLength(userInfo.getGender()) ||
        !StringUtils.hasLength(userInfo.getNickname()) || !StringUtils.hasLength(userInfo.getIntroduction())){
            return AjaxResult.fail(-1,"参数有误!");
        }
        //获取当前登录用户名
        UserInfo currUser = UserSessionUtils.getUser(request);
        String currName = currUser.getUsername();
        userInfo.setUsername(currName);
        int ret = userService.updateInfo(userInfo);
        return AjaxResult.success(ret);
    }

    /*
    * 此方法用于生成验证码
    * */
    @RequestMapping("/getcode")
    public void getcode(HttpServletResponse response, HttpSession session ){
        // 随机生成 4 位验证码
        RandomGenerator randomGenerator = new RandomGenerator("0123456789", 4);
        // 定义图片的显示大小
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(150, 40);
        response.setContentType("image/jpeg");
        response.setHeader("Pragma", "No-cache");//设置页面不缓存当前响应
        AppVariable.textCode = lineCaptcha.getCode().toLowerCase();//每次请求的时候就同步更新记录这个保存验证码的变量 用于后面登录验证 都转为小写
        try {
            // 调用父类的 setGenerator() 方法，设置验证码的类型
            lineCaptcha.setGenerator(randomGenerator);
            // 输出到页面
            lineCaptcha.write(response.getOutputStream());
            // 关闭流
            response.getOutputStream().close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /*
    * 此方法用于发送给用户验证码
    * */
    @RequestMapping("/sendcode")
    public AjaxResult sendCode(String phone) throws Exception {
        if(phone == null){
            return AjaxResult.fail(-1,"参数异常!");
        }
        //首先 根据用户填写的手机号查询数据库
        UserInfo userInfo = userService.getUserByPhone(phone);
        if(userInfo == null){
            return AjaxResult.fail(-2,"手机号异常，未查询到相关用户!");
        }
        //然后根据手机号向用户发送验证码
        String randomCode = UUID.randomUUID().toString().replace("-","").substring(0,4);
        String[] args_ = new String[100];
        Sample.sendMsg(args_,phone,randomCode);
        AppVariable.VerificationCode = randomCode;
        // 设置一个定时任务，1分钟后将验证码清空 保证验证码只在一分钟之内有效
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                AppVariable.VerificationCode = "";
            }
        };
        Timer timer = new Timer();
        timer.schedule(task, 60 * 1000L);
        return AjaxResult.success(null);
    }

    @RequestMapping("/updatepassword")
    public AjaxResult updatePassword(String phone,String code,String newPassword){
        //首先校验参数
        if(!StringUtils.hasLength(phone) || !StringUtils.hasLength(code) || !StringUtils.hasLength(newPassword)){
            return AjaxResult.fail(-1,"参数异常");
        }
        //验证验证码
        if(!code.equals(AppVariable.VerificationCode)){
            return AjaxResult.fail(-2,"验证码错误");
        }
        //然后更新用户密码信息 重新调用加密接口进行加密
        String password = PasswordUtils.encrypt(newPassword);
        //然后将新的密码存入数据库
        int ret = userService.updatePassword(password,phone);
        return AjaxResult.success(ret);
    }

}

