package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.common.UserSessionUtils;
import com.example.demo.entity.ArticleInfo;
import com.example.demo.entity.UserInfo;
import com.example.demo.entity.vo.UserInfoVO;
import com.example.demo.service.ArticleService;
import com.example.demo.service.UserService;
import org.apache.catalina.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 14776
 * Date: 2023-05-25
 * Time: 19:14
 */
@RestController
@RequestMapping("/article")
public class ArticleController {
    @Autowired
    ArticleService articleService;
    @Autowired
    UserService userService;

    @RequestMapping("/mylist")
    public AjaxResult getBlogs(HttpServletRequest request){
        UserInfo userInfo = UserSessionUtils.getUser(request);//拿到当前用户对象
        if(userInfo == null){
            return AjaxResult.fail(-1,"非法请求");
        }
        List<ArticleInfo> ret = articleService.getBlogs(userInfo.getId());
        return AjaxResult.success(ret);
    }

    @RequestMapping("/getuser")
    public AjaxResult getUid(Integer id){
        //通过文章id获取用户信息以及文章总数
        if(id == null){
            return AjaxResult.fail(-1,"参数异常");
        }
        int uid = articleService.getUid(id);//根据文章id查询到用户id
        //知道当前文章的作者了，就可以根据uid去获取用户信息了
        UserInfo userInfo = userService.getUserById(uid);
        UserInfoVO userInfoVO =  new UserInfoVO();
        BeanUtils.copyProperties(userInfo,userInfoVO);
        //再来获取一下当前用户的文章总数
        Integer artCount = articleService.getArtCountByUid(uid);
        userInfoVO.setArtCount(artCount);
        return AjaxResult.success(userInfoVO);
    }

    @RequestMapping("/getblog")
    public AjaxResult getBlog(Integer id){
        //通过blogid获取文章对象展示到博客详情页
        if(id == null){
            return AjaxResult.fail(-1,"参数异常");
        }
        ArticleInfo articleInfo = articleService.getBlog(id);//获取当前文章对象
        return AjaxResult.success(articleInfo);
    }

    @RequestMapping("/delete")
    public AjaxResult delete(Integer id, HttpServletResponse response,HttpServletRequest request) throws IOException {
        System.out.println(id);
        if(id == null){
            return AjaxResult.fail(-1,"参数异常");
        }
        //删除之前需要进行验证 不能删除别的博客
        UserInfo userInfo = UserSessionUtils.getUser(request);//拿到登陆的用户
        int ret = articleService.delete(id,userInfo.getId());
        //我要删除时候不仅是当前blogid正确 并且删除的blog的uid必须等于我的登录用户的id才可以进行删除 否则不行
        return AjaxResult.success(ret);
    }

    @RequestMapping("/uprcount")
    public AjaxResult updateRcount(Integer id){
        if(id == null || id <= 0){
            return AjaxResult.fail(-1,"非法参数");
        }
        return AjaxResult.success(articleService.updateRcount(id));
    }

    @RequestMapping("/add")
    public AjaxResult add(ArticleInfo articleInfo,HttpServletRequest request){
        System.out.println(articleInfo.toString());
        if(articleInfo == null || !StringUtils.hasLength(articleInfo.getTitle()) || !StringUtils.hasLength(articleInfo.getContent())){
            return AjaxResult.fail(-1,"非法参数!");
        }
        //首先获取当前登陆的对象
        UserInfo userInfo = UserSessionUtils.getUser(request);
        if(userInfo == null){
            return AjaxResult.fail(-1,"非法请求!");
        }
        int uid = userInfo.getId();
        articleInfo.setUid(uid);
        int ret = articleService.add(articleInfo);
        return AjaxResult.success(ret);
    }


    @RequestMapping("update")
    public AjaxResult updateArt(ArticleInfo articleInfo,HttpServletRequest request){
        if(articleInfo == null || !StringUtils.hasLength(articleInfo.getTitle()) || !StringUtils.hasLength(articleInfo.getContent())){
            return AjaxResult.fail(-1,"非法参数!");
        }
        //获取当前登录用户
        UserInfo userInfo = UserSessionUtils.getUser(request);
        if(userInfo == null){
            return AjaxResult.fail(-1,"非法请求!");
        }
        int uid = userInfo.getId();
        articleInfo.setUid(uid);//设置uid 后面修改的时候会进行归属人的校验
        return AjaxResult.success(articleService.updateArt(articleInfo));
    }

    @RequestMapping("/listbypage")
    public AjaxResult  pagingQuery(Integer psize,Integer pindex){
        //pageNum是当前页面的页码 psize是每页显示条数
        //首先进行参数的校正 如果前端没传参数 比如第一页的时候需要我们后端处理一下 保证数值是有效的
        if(pindex == null || pindex <= 1){
            pindex = 1;
        }
        if(psize == null || psize <= 1){
            psize = 2;
        }
        //计算数据库查询的offset
        int offsize = (pindex - 1)*psize;//公式推导得出
        List<ArticleInfo> ret = articleService.pagingQuery(psize,offsize);
        //现在同时需要将数据库中数据的总页数返回给前端
        int totalCount = articleService.getCount();
        //那么我们的页数 = totalCount/(psize* 1.0) 然后向上取整
        double count = totalCount/(psize * 1.0);
        //然后进行向上取整
        int pcount = (int) Math.ceil(count);
        HashMap<String,Object> result = new HashMap<>();//定义一个hashmap来包装结果
        result.put("list",ret);
        result.put("pcount",pcount);
        return AjaxResult.success(result);
    }



}
