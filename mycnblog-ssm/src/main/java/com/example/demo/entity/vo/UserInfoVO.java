package com.example.demo.entity.vo;

import com.example.demo.entity.UserInfo;
import lombok.Data;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 14776
 * Date: 2023-05-25
 * Time: 15:36
 */
@Data
public class UserInfoVO extends UserInfo implements Serializable {
    //一个新的vo类 继承了userinfo类的属性 并且有一个新属性 artCount文章总数
    private Integer artCount;
}
