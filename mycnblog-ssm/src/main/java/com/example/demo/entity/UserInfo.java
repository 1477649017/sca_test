package com.example.demo.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 14776
 * Date: 2023-05-25updatetime
 * Time: 10:37
 */
@Data
public class UserInfo implements Serializable {
    private Integer id;//使用Integer兼容新更好 接收null也不会报错
    private String username;
    private String nickname;
    private String password;
    private String photo;
    private LocalDateTime createtime;
    private LocalDateTime updatetime;
    private Integer state;
    private String gender;
    private String introduction;
    private String phone;
}
