package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 14776
 * Date: 2023-05-25
 * Time: 18:39
 */
/*
* 文章的实体类
* */
@Data
public class ArticleInfo implements Serializable {
    private Integer id;
    private String title;
    private String content;
    @JsonFormat(pattern = "yyyy-MM--dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createtime;
    @JsonFormat(pattern = "yyyy-MM--dd HH:mm:ss",timezone = "GMT+8") //设置单个的时间格式化
    private LocalDateTime updatetime;
    private Integer uid;
    private Integer rcount;
    private Integer state;
}
